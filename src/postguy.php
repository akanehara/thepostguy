<?php
require_once 'HTTP/Request2.php';
require_once 'HTTP/Request2/MultipartBody.php';

class PostGuy
{
    static public function get($url)
    {
        return new HTTPGetRequest($url);
    }

    static public function post($url)
    {
        return new HTTPPostRequest($url);
    }

    static public function put($url)
    {
        return new HTTPPutRequest($url);
    }

    static public function delete($url)
    {
        return new HTTPDeleteRequest($url);
    }
}

class HTTPRequest
{
    protected $req;
    protected $params;

    public function __construct($url)
    {
        $this->req    = new HTTP_Request2($url, $this->method());
        $this->params = array();
    }

    protected function method()
    {
        return HTTP_Request2::METHOD_GET;
    }

    public function header($name, $value)
    {
        $this->req->setHeader($name, $value);
        return $this;
    }

    public function headers($headers)
    {
        foreach($headers as $k => $v) {
            $this->header($k, $v);
        }
        return $this;
    }

    public function param($name, $value)
    {
        $this->params[$name] = $value;
        return $this;
    }

    public function params($params)
    {
        foreach($params as $k => $v) {
            $this->param($k, $v);
        }
        return $this;
    }

    public function send()
    {
    }
}

class HTTPGetRequest extends HTTPRequest
{
    public function __construct($url)
    {
        parent::__construct($url);
    }

    protected function method()
    {
        return HTTP_Request2::METHOD_GET;
    }

    public function send()
    {
        $url = $this->req->getUrl();
        foreach($this->params as $k => $v) {
            $url->setQueryVariable($k, $v);
        }
        return $this->req->send();
    }
}

class HTTPPostRequest extends HTTPRequest
{
    protected $files;

    public function __construct($url)
    {
        parent::__construct($url);
        $this->files = array();
    }

    protected function method()
    {
        return HTTP_Request2::METHOD_POST;
    }

    public function file($name, $filepath, $filename="", $contentType="")
    {
        $this->files[$name] = array($filepath,  $filename, $contentType);
        return $this;
    }

    public function send()
    {
        if (0 < count($this->files)) {
            return $this->sendForceMultipart();
        }
        foreach ($this->params as $k => $v) {
            $this->req->addPostParameter($k, $v);
        }
        return $this->req->send();
    }

    public function sendForceMultipart()
    {
        $fps = array();
        $uploads = array();
        foreach ($this->files as $k => $v) {
            list($filepath, $filename, $contentType) = $v;
            $fp = fopen($filepath, 'r');
            $fps[] = $fp;
            $e = array();
            $e['fp']       = $fp;
            $e['size']     = filesize($filepath);
            $e['filename'] = $filename;
            $e['type']     = $contentType;
            $uploads[$k]   = $e;
        }
        $mbody = new HTTP_Request2_MultipartBody($this->params, $uploads);
        $this->req->setBody($mbody);
        return $this->req->send();
    }
}

class HTTPPutRequest extends HTTPPostRequest
{
    protected function method()
    {
        return HTTP_Request2::METHOD_PUT;
    }
}

class HTTPDeleteRequest extends HTTPGetRequest
{
    protected function method()
    {
        return HTTP_Request2::METHOD_DELETE;
    }
}


