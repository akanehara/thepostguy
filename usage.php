<?php
require_once 'postguy.php';

$URL = "http://localhost/test/";

$res = PostGuy::put($URL)
    ->header('User-Agent', 'PostGuy');
    ->param('method', 'put')
    ->file('php', './put.php', 'hoge.php', 'text/plain')
    ->send()
    ;
echo $res->getBody();

echo "------------------------------------------------------\n";

$res = PostGuy::post($URL)
    ->header('User-Agent', 'PostGuy');
    ->param('method', 'post')
    ->file('php', './put.php', 'hoge.php', 'text/plain')
    ->send()
    ;
echo $res->getBody();

echo "------------------------------------------------------\n";

$res = PostGuy::get($URL)
    ->header('User-Agent', 'PostGuy');
    ->param('method', 'get')
    ->send()
    ;
echo $res->getBody();

echo "------------------------------------------------------\n";

$res = PostGuy::delete($URL)
    ->header('User-Agent', 'PostGuy');
    ->param('method', 'delete')
    ->send()
    ;
echo $res->getBody();

